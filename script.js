const userApi = 'https://ajax.test-danit.com/api/json/users'
const postApi= 'https://ajax.test-danit.com/api/json/posts'

class Card {
    constructor(postId, title, body, userId, name, email) {
        this.postId = postId
        this.title = title
        this.body = body
        this.userId = userId
        this.name = name
        this.email = email
    }

    // Создание карточки
    render() {
        const card = document.createElement('div')

        card.id = `${this.postId}`
        card.classList.add('card')
        card.innerHTML = `
            <h2 class="cardTitle">${this.title}</h2>
            <span class="cardText">${this.body}</span>
            <span class="cardUserName"><br><b>Author:</b> ${this.name}</span>
            <span class="cardUserMail"><br><b>E-mail:</b> ${this.email}</span>
            <div class="cardBtn">
            <button onclick="deleteCard(${this.postId})"><b>Delete</b></button>
            </div>
        `
        return card
    }
}

// Удаление карточки
function deleteCard(postId) {
    fetch(`${postApi}/${postId}`, {
        method: 'DELETE',
    })
        .then(response => {
            if (response.ok) {
                document.getElementById(`${postId}`).remove()
            } else {
                console.waen(`Failed to delete card with postId: ${postId}`)
            }
        })
        .catch(err => console.warn(err))
}

//Получение публикаций и пользователей
fetch(`${userApi}`)
    .then(response => response.json())
    .then(users => {
        fetch(`${postApi}`)
            .then(response => response.json())
            .then(posts => {
              
                const cardsWrapper = document.querySelector('#cardsWrapper')
                posts.forEach(post => {
                    const user = users.find(user => user.id === post.userId)
                    const card = new Card(
                        post.id,
                        post.title,
                        post.body,
                        user.id,
                        user.name,
                        user.email
                    )
                    cardsWrapper.append(card.render())
                })
            })
            .catch(err => console.warn(err))
    })
    .catch(err => console.warn(err))

    